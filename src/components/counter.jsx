import React, { Component } from "react";

class Counter extends Component {
  state = {
    count: this.props.value,
  };

  styles = {
    fontSize: 20,
    fontWeight: "bold",
  };

  formateCounter() {
    const { count } = this.state;

    return count == 0 ? <h5>Zero</h5> : count;
  }


  handleIncrement = () => {
    this.setState({ count: this.state.count + 1 });
  };

  render() {

    return (
      <div>
        <span style={this.styles} className={this.getBadgeClasses()}>
          {this.formateCounter()}
        </span>
        <button
          onClick={this.handleIncrement}
          className="btn btn-secondary btn-sm"
        >
          increment
        </button>
        <button
          onClick={() => this.props.onDelete(this.props.id)}
          className="btn btn-danger btn-sm  m-2"
        >
          Delete
        </button>
      </div>
    );
  }

  getBadgeClasses() {
    let classes = "badge m-2 badge-";
    classes += this.state.count == 0 ? "warning" : "primary";
    return classes;
  }
}

export default Counter;
